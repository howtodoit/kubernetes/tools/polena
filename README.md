# Polena

The purpose of **polena** is to provide a lightweight, web based, simple to install and easy to use tool to access the Kubernetes logs. 
It is based on [stern/stern](https://github.com/stern/stern) and [gvalkov/tailon](https://github.com/gvalkov/tailon).

## Supported deploys

**Polena** supports the following deploy methods:

* [docker-compose](deploy/docker/README.md)
* [k8s manifest files](deploy/k8s/README.md)
* [Helm chart](deploy/helm/README.md)
## License
For open source projects, say how it is licensed.


