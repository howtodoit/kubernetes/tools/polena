## Kubernetes

[Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/) is a portable, extensible, open-source platform for managing containerized workloads and services, that facilitates both declarative configuration and automation. 

## Basic usage

```shell
 kubectl apply -f .
 ```

You can deploy polena in a specific namespace.

```shell
kubectl create ns polena
kubectl -n polena apply -f .
```

or 

```shell
docker-compose --env-file env -p polena up -d
```
You should customize your deploy by editing `kubeconfig-cm.yaml` to use your kube config file. To change the log query and max file log size you have to edit the config map `query-cm.yaml`. 
You can choose the values of:
* **QUERY**: the query for stern, eg: `-n my-namespace ".*"`
* **MAX_FILE_SIZE**: the max log file size. If the file `stern.log' exced this limiti it will be truncated.
for your enviroment.

Edit `ingress.yaml` to expose polena properly.