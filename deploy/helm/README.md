## Helm

[Helm](https://helm.sh/) is the best way to find, share, and use software built for Kubernetes.

## Basic usage

Add the repo:

```shell
helm repo add polena https://gitlab.com/api/v4/projects/32916218/packages/helm/stable  
helm search repo polena  
```

Create the helm values file from the default one and edit it to customize you installation:

```shell
helm inspect values polena/polena > custom-values.yaml  
```
You can copy and paste /root/.kube/config file content or use utility bash scripts to auto populate helm values with your /root/.kube/config file content:

```shell
#Prepare copy of kube config file
sed -e 's/^/  /' /root/.kube/config > config.bck
#Prepare filled copy of helm values
sed -e "/#INSERT_KUBECONFIG_HERE#/r config.bck" -e "/#INSERT_KUBECONFIG_HERE#/d" "custom-values.yaml" > "filled-custom-values.yaml"
#Clean
rm config.bck custom-values.yaml
```

Install **polena**

```shell
kubectl create ns polena
helm -n polena install polena "polena/polena" --version x.y.z -f filled-custom-values.yaml  
```
