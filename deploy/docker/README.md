## Docker compose

[Docker compose](https://docs.docker.com/compose/) is a tool for defining and running multi-container Docker applications.

## Basic usage

```shell
 docker-compose -p polena up
 ```

You can use an environment file to customize the polena behavior.

```shell
docker-compose --env-file env -p polena up
```

or 

```shell
docker-compose --env-file env -p polena up -d
```

to run the stack in detached mode.

You can set the following environment variables:
* **QUERY**: the query for stern, eg: `-n my-namespace ".*"`
* **CONFIG**: the kube config full path, eg: `/my/path/config`
* **PORT**: the tailon port, eg: `80`
* **MAX_FILE_SIZE**: the max log file size. If the file `stern.log' exced this limiti it will be truncated.
